import { Note } from "./types";

const api = {
    notes: [
      {
        id: 'nota',
            title: "Un Titulo",
            lastEdited: "10/10/2022",
            archived: false,
            content: "Ingrese texto aqui",
            categories: 'random'
      },
      {
        id: 'nota2',
            title: "Un Titulo 2",
            lastEdited: "10/10/2022",
            archived: false,
            content: "Ingrese texto aqui",
            categories: 'random'
      },
      {
        id: 'nota3',
            title: "Un Titulo 3",
            lastEdited: "10/10/2022",
            archived: false,
            content: "Ingrese texto aqui",
            categories: 'random'
      },
    ]
};

  export default api;