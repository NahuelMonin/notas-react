import { useState, useMemo, useEffect } from "react";
import api from "./api";
import NoteCard from "./components/NoteCard";
import NoteModal from "./components/NoteModal";
import { Note } from "./types";
import { useLocalStorage } from "./useLocalStorage";

function App() {
  const [notes, setNotes] = useLocalStorage("notas", api.notes);
  const [draft, setDraft] = useState<null | Partial<Note>>(null);
  const [view, setView] = useState<"active" | "archived">("active");
  const [category, setCategory] = useState<string>("Todas");
  const matches = useMemo(() => {
    return notes.filter((note: Note) => {
      if (category === "Todas") {
        if (view === "active") {
          return !note.archived;
        } else if (view === "archived") {
          return note.archived;
        }
      } else {
        if (view === "active") {
          return !note.archived && note.categories.includes(category);
        } else if (view === "archived") {
          return note.archived && note.categories.includes(category);
        }
      }
    });
  }, [notes, view, category]);

  useEffect(() => {
    setNotes(notes)
  },[]);

  function handleEdit(note: Note) {
    setDraft(note);
  }

  function handleDelete(id: Note["id"]) {
    setNotes(notes.filter((note: Note) => note.id !== id));
  }

  function handleArchive(id: Note["id"]) {
    setNotes((notes: Array<Note>) => {
      return notes.map((note) => {
        return note.id !== id ? note : { ...note, archived: !note.archived };
      });
    });
  }

  function handleDraftChange(field: string, value: string) {
    setDraft((draft) => ({
      ...draft,
      [field]: value,
    }));
  }

  function handleSave() {
    let updatedList = [];
    
    if (draft?.id) {
      updatedList = notes.map((note: Note) => {
        return note.id !== draft.id ? note : {...draft, lastEdited: new Date().toLocaleDateString()} as Note; 
      })
    } else {
      updatedList = notes.concat({
        id: crypto.randomUUID(),
        lastEdited: new Date().toLocaleDateString(),
        ...draft as Omit<Note, 'id' | 'lastEdited'> 
      })
    }
    
    setDraft(null);
    return setNotes(updatedList);
  }

  return (
    <main>
      <div style={{ marginBottom: 24 }}>
        <h1>Mis Notas</h1>
        <div style={{ display: "flex", gap: 24 }}>
          <button
            onClick={() => setDraft({title: 'Nueva Nota'})}
            className="nes-btn  is-success"
          >
            Crear nota
          </button>
          <button
            className="nes-btn"
            onClick={() => {
              setView(view === "active" ? "archived" : "active");
            }}
          >
            {view === "active" ? "Ver archivadas" : "Ver activas"}
          </button>
          <div className="nes-select" style={{ maxWidth: "250px" }}>
            <select id="default_select">
              <option value="" disabled selected hidden>
                Categoria...
              </option>
            </select>
          </div>
        </div>
      </div>
      <div
        style={{
          display: "grid",
          gap: 24,
          gridTemplateColumns: "repeat(auto-fill, minmax(500px, 1fr)",
        }}
      >
        {matches.map((note: Note) => (
          <NoteCard
            onEdit={handleEdit}
            onDelete={handleDelete}
            onArchive={handleArchive}
            key={note.id}
            note={note}
          />
        ))}
      </div>
      {draft && (
        <NoteModal
          note={draft}
          onClose={() => {
            setDraft(null);
          }}
          onChange={handleDraftChange}
          onSave={handleSave}
        />
      )}
    </main>
  );
}

export default App;
