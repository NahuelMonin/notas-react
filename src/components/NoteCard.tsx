import { Note } from "../types";

type Props = {
  note: Note;
  onArchive: (id: Note["id"]) => void;
  onDelete: (id: Note["id"]) => void;
  onEdit: (note: Note) => void;
};

function NoteCard({ note, onArchive, onDelete, onEdit }: Props) {
  return (
    <div className="nes-container">
      <h3>{note.title}</h3>
      <p>Ultima edicion: {note.lastEdited}</p>
      <p>{note.content}</p>
      <div style={{ display: "flex", gap: 12 }}>
        <button className="nes-btn" onClick={() => onArchive(note.id)}>
          {note.archived ? "Desarchivar" : "Archivar"}
        </button>
        <button className="nes-btn is-primary" onClick={() => onEdit(note)}>
          Editar
        </button>
        <button className="nes-btn is-error" onClick={() => onDelete(note.id)}>
          Borrar
        </button>
      </div>
      <div style={{ paddingTop: 10 }}>
        <span className="nes-text is-error">{note.categories}</span>
      </div>
    </div>
  );
}

export default NoteCard;
