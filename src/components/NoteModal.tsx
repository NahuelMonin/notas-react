import { Note } from "../types";

type Props = {
    note: Partial<Note>;
    onClose: VoidFunction;
    onChange: (field: string, value: string) => void;
    onSave: VoidFunction;
  };
  
  function NoteModal({note, onClose, onChange, onSave}: Props) {
    return (
      <section 
        className="nes-dialog is-rounded" 
        style={{width: '100vw', height: '100vh', position: 'fixed', top: 0, left: 0, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <div style={{position: 'absolute', backgroundColor: 'rgba(0,0,0,0.2)', width: '100%', height: '100%'}}/>
          <form method="dialog" style={{backgroundColor: 'white', zIndex: 1, padding: 12, border: '5px solid black'}}>
            <h1 className="title">Crear / Editar Nota</h1>
            <div className="nes-field">
              <label htmlFor="title">Titulo</label>
              <input 
                className="nes-input" 
                value={note.title}
                onChange={(event) => {onChange('title', event.target.value)}} 
                type="text" 
                id="title" 
              /> 
            </div>
            <div className="nes-field">
              <label htmlFor="content">Contenido</label>
              <textarea 
                id="content" 
                className="nes-textarea" 
                value={note.content} 
                onChange={(event) => {onChange('content', event.target.value)}} 
              />
            </div>
            <div className="nes-field">
              <label htmlFor="categories"> Categoria </label>
              <input 
                className="nes-input" 
                id="categories" 
                type="text" 
                value={note.categories}
                onChange={(event) => {onChange('categories', event.target.value)}} 
              />
            </div>
            <div style={{marginTop: 24, display: 'flex', alignItems: 'center', justifyContent: 'space-between',  }}>
              <button onClick={onClose} className="nes-btn">Cancelar</button>
              <button onClick={onSave} className={note.id? "nes-btn is-primary" : "nes-btn is-success"}> Guardar </button>
            </div>
          </form>
      </section>
    );
  }

  export default NoteModal;